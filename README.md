SDA projektas1; Java version 8
programa nuskaito darbuotoju duomenis ir iseda i ekrana lenteles pavidalu, 
galimi veiksmai: 
1. istrinti atskira darbuotoja (mygtukas "Delete" lenteleje)
2. pakeisti konretaus darbuotojo duomenis (mygtukas "Edit" lenteleje)
3. ivesti nauja darbuotoja (mygtukas "Edit new employee")
4. atlikti paieska pagal ivesta teksta (mygtukas "Search"). Paiskos duomenys pateikiami atskirame lange; niekaip niekaip neisaugomi ar pan.

SQLscript:
create table dbmusu.EMPLOYEES (
id_employees INT(6) UNSIGNED auto_increment primary key, 
first_name VARCHAR(30) not null,
last_name VARCHAR(30) not null,
id_department INT(6) not null, 
manager INT(6)
);