package FinalProject1.controller;

import FinalProject1.model.Employee;
import FinalProject1.repositories.EmployeeRepository;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AddButton {

    public static void addEditButtonToTable(TableView table) {
        // TableColumn<Employee, Void> colBtn = new TableColumn("Button Column");
        TableColumn<Employee, Void> colBtn = new TableColumn();

        Callback<TableColumn<Employee, Void>, TableCell<Employee, Void>> cellFactory = new Callback<TableColumn<Employee, Void>, TableCell<Employee, Void>>() {
            @Override
            public TableCell<Employee, Void> call(final TableColumn<Employee, Void> param) {
                final TableCell<Employee, Void> cell = new TableCell<Employee, Void>() {

                    private final Button btn = new Button("Edit");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Employee oldEmployee = getTableView().getItems().get(getIndex());
                            System.out.println("selected Employee: " + oldEmployee.getId_employees());
                            WindowOpen.editWindowOpen(oldEmployee);//
                            table.setItems(ReadDB.getNewEmployeeList()); // refreshina langa!!!!!!!!!!!!!!!!!!! // refreshina langa!!!!!!!!!!!!!!!!!!!!
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        table.getColumns().add(colBtn);
    }

    public static void addDeleteButtonToTable(TableView table) {
        // TableColumn<Employee, Void> colBtn = new TableColumn("Button Column");
        TableColumn<Employee, Void> colBtn = new TableColumn();

        Callback<TableColumn<Employee, Void>, TableCell<Employee, Void>> cellFactory = new Callback<TableColumn<Employee, Void>, TableCell<Employee, Void>>() {
            @Override
            public TableCell<Employee, Void> call(final TableColumn<Employee, Void> param) {
                final TableCell<Employee, Void> cell = new TableCell<Employee, Void>() {

                    private final Button btn = new Button("Delete");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Employee employee = getTableView().getItems().get(getIndex());
                            System.out.println("selectedData: " + employee.getId_employees());
                            System.out.println("ka darom ?");
                            EmployeeRepository.deleteSelectedEmployee(employee.getId_employees()); // VEIKIA!!!
                            table.setItems(ReadDB.getNewEmployeeList()); // refreshina langa!!!!!!!!!!!!!!!!!!!
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        table.getColumns().add(colBtn);
    }
}
