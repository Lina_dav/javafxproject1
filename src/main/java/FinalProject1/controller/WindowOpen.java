package FinalProject1.controller;

import FinalProject1.model.Employee;
import FinalProject1.repositories.EmployeeRepository;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class WindowOpen {
    public static void secondWindowOpen(String windowTitle, String textForSearch, ObservableList employeeList) {
//    public static TableView secondWindowOpen(Stage primaryStage) {
//         Label secondLabel = new Label(windowTitle);
        Stage newWindow = new Stage(); // naujas langas
        newWindow.initModality(Modality.APPLICATION_MODAL);
        newWindow.setTitle(windowTitle);

        // Set position of second window, related to primary window.
        //  newWindow.setX(primaryStage.getX() + 200);
        //  newWindow.setY(primaryStage.getY() + 100);
        // newWindow.setX(200);
        // newWindow.setY(100);
        newWindow.setMinWidth(250);
        TableView tableSearch = new TableView();

        // tableSearch.setEditable(true);
        // TableColumn noCol = new TableColumn("No.");
        // TableColumn<Employee, Void> noCol = new TableColumn("Index", person -> null, 50);
        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        TableColumn lastNameCol = new TableColumn("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("last_name"));
        TableColumn idCol = new TableColumn("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id_employees"));
        // TableColumn emailCol = new TableColumn("Email");

        tableSearch.getColumns().addAll(firstNameCol, lastNameCol, idCol);
        tableSearch.setItems(EmployeeRepository.searchResultList(textForSearch, employeeList));
        // secondTable.setItems(employeeList);
        // New window (Stage)

        Button closeButton = new Button(" CLOSE ");
        closeButton.setOnAction(e -> newWindow.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(tableSearch, closeButton);
        layout.setAlignment(Pos.CENTER); // button centre

        Scene scene = new Scene(layout);
        newWindow.setScene(scene);
        //Scene secondScene = new Scene(tableSearch, 400, 200);
        //newWindow.setScene(secondScene);

        newWindow.showAndWait();
    }

    public static void editWindowOpen(Employee oldEmployee) {
        // Label secondLabel = new Label("Search result ");
        Stage newWindow = new Stage();
        newWindow.setTitle(" Edit ");
        newWindow.setResizable(true);

        GridPane editGridPane = new GridPane();
        Button button = new Button(" Edit Employee data");
        Scene editScene = new Scene(editGridPane, 600, 100);

        TextField textFieldName = new TextField(oldEmployee.getFirst_name());
        TextField textFieldSurname = new TextField(oldEmployee.getLast_name());
        TextField textFieldID = new TextField(Integer.toString(oldEmployee.getId_employees()));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setMaxWidth(150);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setMaxWidth(300);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setMaxWidth(40);
        ColumnConstraints column4 = new ColumnConstraints();
        column4.setMaxWidth(200);
        editGridPane.getColumnConstraints().addAll(column1, column2, column3, column4);
        // root.getColumnConstraints().addAll(column1, column2, column3);

        // Adding controls to specified cells. Second argument is column index third one is the row index.
        editGridPane.add(textFieldName, 0, 0);
        editGridPane.add(textFieldSurname, 1, 0);
        // root.add(textFieldID, 2, 0);
        editGridPane.add(button, 3, 0);

        editGridPane.add(new Label("First Name"), 0, 1);
        editGridPane.add(new Label("Last Name"), 1, 1);

        button.setOnAction(ActionEvent -> {
            EmployeeRepository.editEmployeeList(oldEmployee, textFieldName.getText(), textFieldSurname.getText());
            newWindow.close();
        });

        newWindow.setScene(editScene);
        newWindow.show();
        // return editGridPane;
    }
}
