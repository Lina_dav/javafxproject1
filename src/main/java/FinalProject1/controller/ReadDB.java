package FinalProject1.controller;

import FinalProject1.model.Employee;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.List;

import static FinalProject1.controller.SessionManager.shutdown;

public class ReadDB {
    //darbuotoju sarasas:
    public static ObservableList<Employee> getNewEmployeeList() {
        Session session = SessionManager.getSessionFactory().openSession();
        String selectAllEmployeesHQL = "from Employee";
        Query selectAllEmployeesQuery = session.createQuery(selectAllEmployeesHQL);
        List<Employee> employeeListTemp = selectAllEmployeesQuery.getResultList();
        return FXCollections.observableArrayList(employeeListTemp);
    }
}
