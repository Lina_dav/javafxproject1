package FinalProject1.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@ToString
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id_employees;

    @Column
    private String first_name;
    @Column
    private String last_name;
    @Column
    private int id_departments;
    @Column
    private int manager;


    /* public Employee(int id_employees, String first_name, String last_name, int id_departments, int manager) {
         this.id_employees = id_employees;
         this.first_name = first_name;
         this.last_name = last_name;
         this.id_departments = id_departments;
         this.manager = manager;
     }
     */

    public void setEmployee(String first_name, String last_name, int id_departments, int manager) {
        first_name = firstLetterUpperCase(first_name);
        last_name = firstLetterUpperCase(last_name);
       // this.id_employees = id_employees;
        this.first_name = first_name;
        this.last_name = last_name;
        this.id_departments = id_departments;
        this.manager = manager;
    }

    public boolean searchMatch(String searchText, Employee employee) {
        return employee.getFirst_name().toLowerCase().contains(searchText.toLowerCase())
                || employee.getLast_name().toLowerCase().contains(searchText.toLowerCase());
    }

    private String firstLetterUpperCase(String text) {
        String firstLetterUpperCase = text.substring(0, 1).toUpperCase();
        return firstLetterUpperCase + text.substring(1);
    }
}

