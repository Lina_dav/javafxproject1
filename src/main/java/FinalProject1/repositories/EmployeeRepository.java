package FinalProject1.repositories;

import FinalProject1.controller.SessionManager;
import FinalProject1.model.Employee;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRepository {
    public static void editEmployeeList(Employee oldEmployee, String edit_first_name, String edit_last_name) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            System.out.println("Darbuotojas " + oldEmployee.getLast_name());
            oldEmployee.setFirst_name(edit_first_name);
            oldEmployee.setLast_name(edit_last_name);
            session.update(oldEmployee);
            transaction.commit();
            session.close();

        } catch (Exception e) {
            System.out.println("error while editing");
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public static void saveNewEmployee(String first_name, String last_name, int id_department, int manager) {
        Session session = SessionManager.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Employee employee = new Employee();
        employee.setEmployee(first_name, last_name, id_department, manager);
        session.save(employee);
        transaction.commit();
        session.close();
    }

    // delete selected https://stackoverflow.com/questions/36066102/delete-selected-row-from-tableview-and-the-effective-use-of-scenebuilder-in-jav
    public static void deleteSelectedEmployee(int index) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Employee employee = session.find(Employee.class, index);
            System.out.println("Project " + employee.getLast_name() + " deleted");
            session.delete(employee);
            transaction.commit();
            session.close();
            System.out.println("darbuotojas istrintas");
        } catch (Exception e) {
            System.out.println("error while employee delete");
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public static ObservableList<Employee> searchResultList(String textSearch, List<Employee> employeeList) {
        List<Employee> searchResultList = new ArrayList<>();
        for (Employee emp : employeeList
        ) {
            if (emp.searchMatch(textSearch, emp)) {
                searchResultList.add(emp);
            }
        }
        return FXCollections.observableArrayList(searchResultList);
    }
}