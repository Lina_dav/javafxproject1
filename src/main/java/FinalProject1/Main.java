package FinalProject1;

import FinalProject1.controller.AddButton;
import FinalProject1.controller.ReadDB;
import FinalProject1.controller.WindowOpen;
import FinalProject1.model.Employee;
import FinalProject1.repositories.EmployeeRepository;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import static FinalProject1.controller.SessionManager.shutdown;

public class Main extends Application {

    private Scene scene;
    private static TableView table = new TableView();
    private static ObservableList<Employee> employeeList;

    public static void main(String[] args) {
        Main.launch(); // JavaFX
        shutdown();
    }

    // TableViewSample(primaryStage); // https://docs.oracle.com/javafx/2/ui_controls/table-view.htm
    public void TableView(Stage stage) {
        // nuskaito is duomenu bazes darbuotoju sarasa:
        employeeList = ReadDB.getNewEmployeeList();

        Scene scene = new Scene(new Group());
        stage.setTitle("Table View "); //textas lango juostoje
        stage.setWidth(600);
        stage.setHeight(700);

        final Label label = new Label("List of Employees: "); // textas virs lenteles
        label.setFont(new Font("Arial", 20));

        GridPane root1 = new GridPane();
        Button buttonSearch = new Button(" Search ");
        TextField textForSearch = new TextField();
        ColumnConstraints columnSearch = new ColumnConstraints();
        columnSearch.setMaxWidth(700);
        ColumnConstraints columnButton = new ColumnConstraints();
        columnButton.setMaxWidth(100);
        root1.getColumnConstraints().addAll(columnSearch, columnButton);
        root1.add(textForSearch, 0, 0);
        root1.add(buttonSearch, 1, 0);

        buttonSearch.setOnAction(ActionEvent -> {
            WindowOpen.secondWindowOpen("Search results",textForSearch.getText(), employeeList);
        });
        table.setEditable(true);
        TableColumn idCol = new TableColumn("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id_employees"));
        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        TableColumn lastNameCol = new TableColumn("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("last_name"));
        // TableColumn emailCol = new TableColumn("Email");
        table.getColumns().addAll(firstNameCol, lastNameCol, idCol);
        // table.getColumns().addAll(firstNameCol, lastNameCol);

        AddButton.addDeleteButtonToTable(table);
        AddButton.addEditButtonToTable(table);
        table.setItems(employeeList);
        // System.out.println("pirmas narys: " + employeeList.get(0).getLast_name());

        // Add new Employee :
        GridPane root = new GridPane();
        Button buttonAddNew = new Button(" Add new Employee ");
        TextField textFieldName = new TextField();
        TextField textFieldSurname = new TextField();
        TextField textFieldID = new TextField();
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setMaxWidth(150);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setMaxWidth(300);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setMaxWidth(40);
        ColumnConstraints column4 = new ColumnConstraints();
        column4.setMaxWidth(200);
        root.getColumnConstraints().addAll(column1, column2, column3, column4);
        // root.getColumnConstraints().addAll(column1, column2, column3);

        // Adding controls to specified cells. Second argument is column index third one is the row index.
        root.add(textFieldName, 0, 0);
        root.add(textFieldSurname, 1, 0);
        // root.add(textFieldID, 2, 0);
        root.add(buttonAddNew, 3, 0);

        root.add(new Label("First Name"), 0, 1);
        root.add(new Label("Last Name"), 1, 1);
        // root.add(new Label("ID"), 2, 1);

        buttonAddNew.setOnAction(ActionEvent -> {
           EmployeeRepository.saveNewEmployee(textFieldName.getText(), textFieldSurname.getText(), 2, 1);
            employeeList = ReadDB.getNewEmployeeList();
            table.setItems(employeeList); // refreshina langa!!!!!!!!!!!!!!!!!!!!
        });

        // https://www.youtube.com/watch?v=W2SjWBMRCgE
        //  https://riptutorial.com/javafx/example/27946/add-button-to-tableview

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, root1, table, root);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void init() throws Exception {
        System.out.println("Initializing app");
    }

    @Override
    public void start(Stage primaryStage) {
        System.out.println("Starting app");
        TableView(primaryStage); // https://docs.oracle.com/javafx/2/ui_controls/table-view.htm
    }

    @Override
    public void stop() throws Exception {
        // this will be executed even if Exception is thrown at runtime
        System.out.println("Stopping app");
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Scene getScene() {
        return this.scene;
    }

}
